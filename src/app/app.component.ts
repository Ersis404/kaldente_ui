import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserInfoService } from './services/user-info.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'KALDENTE';

  get userIsLoggedIn() {
    return this.userInfoService.isLoggedIn();
  }

  constructor(private userInfoService: UserInfoService) { }

  ngOnInit(): void { }
}
