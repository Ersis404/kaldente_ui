import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorizationComponent } from './logIn/authorization/authorization.component';
import { LogoutComponent } from './logIn/logout/logout.component';
import { CompComponent } from './comp/comp.component';
import { ConfirmEmailCodeComponent } from './logIn/authorization/confirm-email-code/confirm-email-code.component';


const routes: Routes = [

  { path: 'login', loadChildren: () => import('./logIn/logIn.module').then(m => m.LogInModule) },
  { path: 'logout', component: LogoutComponent },
  { path: 'CompComponent', component: CompComponent },
  { path: 'activateEmail/:activationCode', component: ConfirmEmailCodeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
