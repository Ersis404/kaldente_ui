import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmEmailCodeComponent } from './confirm-email-code.component';

describe('ConfirmEmailCodeComponent', () => {
  let component: ConfirmEmailCodeComponent;
  let fixture: ComponentFixture<ConfirmEmailCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmEmailCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmEmailCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
