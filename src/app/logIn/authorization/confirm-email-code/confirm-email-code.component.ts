import { LoginService } from 'src/app/services/login.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-confirm-email-code',
  templateUrl: './confirm-email-code.component.html',
  styleUrls: ['./confirm-email-code.component.scss']
})
export class ConfirmEmailCodeComponent implements OnInit {

  params: any = this.route.snapshot.params;
  isActive: boolean = false;

  constructor(private route: ActivatedRoute,
              private loginService: LoginService,
              private router: Router) { }

  ngOnInit(): void {
    this.loginService.activateEmail(this.params.activationCode).subscribe(res => {
      this.isActive = res.message == 'email confirmed';
    });
  }

  close(): void {
    this.router.navigate(['/login']);
  }
}
