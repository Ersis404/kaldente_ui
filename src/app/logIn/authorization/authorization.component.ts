import { LoginService } from './../../services/login.service';
import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user/User';


@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS,
    useValue: {showError: true}
  }]
})
export class AuthorizationComponent implements OnInit {

  loadRegister = false;
  toRegister = false
  confrimPassword: string;
  usernameIsBusy: boolean = false;
  checkUsername: string;

  errorMessage: string = '';
  user: User = new User();


  fullNameFormGroup: FormGroup;
  phoneFormGroup: FormGroup;
  emailFormGroup: FormGroup;
  passwordFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder,
              public dialog: MatDialog,
              private loginService: LoginService,
              private router: Router) { }

  ngOnInit(): void {

    // console.log(this.params)
    // if (this.params.activationCode != 'null') {
    //   alert(1);
    //   this.loginService.activateEmail(this.params.activationCode).subscribe(res => {});
    // }

    this.fullNameFormGroup = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.nullValidator]
    });

    this.emailFormGroup = this.formBuilder.group({
      email: ['', Validators.email],
      secretWord: ['', Validators.required]
    });

    this.passwordFormGroup = this.formBuilder.group({
      userName: ['', Validators.required],
      passwordRegister: ['', Validators.required],
      confrimPassword: ['', Validators.required]
    });

    this.phoneFormGroup = this.formBuilder.group({
      phone: ['', Validators.nullValidator],
      gender: ['', Validators.nullValidator]
    });
  }

  toRegisterOrLogInShow(): void {
    if (this.loadRegister) { return; }
    this.loadRegister = true;
    this.toRegister = !this.toRegister;
    if (this.toRegister) {
      $('#toRegister').toggleClass('toRegisterMove');
      $('#img').toggleClass('opacity');
      $('#block-intro').toggleClass('blockMoveRight');
      $('#text').toggleClass('opacity');

      setTimeout(() => {
        $('#img').toggleClass('hide');
        $('#text').toggleClass('hide');
        $('#content-register').toggleClass('hide');
        $('#block-inputs-form').toggleClass('hide');
        $('#block-intro').toggleClass('blockMoveRight2 right');

        setTimeout(() => {
          $('#toRegister').toggleClass('toRegisterMove rotate');
          $('#text').toggleClass('opacity');
          this.loadRegister = false;
        }, 1200);
      }, 1500);
    } else {
      $('#text').toggleClass('opacity');
      $('#block-intro').toggleClass('blockMoveRight2');
      $('#toRegister').toggleClass('toRegisterMove rotate');
      $('#img').toggleClass('hide');

      setTimeout(() => {
        $('#content-register').toggleClass('hide');
        $('#block-inputs-form').toggleClass('hide');
        $('#block-intro').toggleClass('blockMoveRight right');
        $('#img').toggleClass('opacity');

        setTimeout(() => {
          $('#text').toggleClass('hide');
          $('#text').toggleClass('opacity');
          $('#toRegister').toggleClass('toRegisterMove');
          this.loadRegister = false;
        }, 1000);
      }, 1500);
    }
  }

  confrimEmail(): void {
    const confrimEmail = this.dialog.open(ConfirmEmailComponent, {
      width: '400px',
      data: this.user
      });
    confrimEmail.afterClosed().subscribe(result => {
      if (!result) { this.toRegisterOrLogInShow(); }
    });
  }

  login() {
    this.loginService.getToken(this.user).subscribe(resp => {
      console.log(resp);
      this.router.navigate(['/CompComponent']);
      if (resp.user === undefined || resp.user.token === undefined || resp.user.token === 'INVALID') {
            return;
          }
        },
      errResponse => {
        switch (errResponse.status) {
          case 401:
          case 403:
            this.errorMessage = 'Неверное имя пользователя или пароль';
            break;
          case 404:
            this.errorMessage = 'Страница не найдена';
            break;
          case 408:
            this.errorMessage = 'Истекло вря ожидания';
            break;
          case 500:
            this.errorMessage = 'Internal Server Error';
            break;
          default:
            this.errorMessage = 'Server Error';
            break;
        }
        setTimeout(() => this.errorMessage = '', 5000);
      }
    );
  }

  checkLogin(): void {
    this.checkUsername = this.user.username;
    this.loginService.checkLogin(this.checkUsername).subscribe(res => {
      this.usernameIsBusy = res.message == 'username is busy';
    });
  }
}


