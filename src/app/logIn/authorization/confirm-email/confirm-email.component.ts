import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as $ from 'jquery';
import { LoginService } from 'src/app/services/login.service';


@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.scss']
})
export class ConfirmEmailComponent implements OnInit {

  message: string;
  isLoadAnswer: boolean = false;
  emailConfirmed: boolean;

  constructor(public dialogRef: MatDialogRef<ConfirmEmailComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private loginService: LoginService) { }

  ngOnInit(): void {
    this.loginService.register(this.data).subscribe(res => {
      console.log(res)
      this.isLoadAnswer = true;
      this.emailConfirmed = res.message == 'Account already exists';
      this.message = !this.emailConfirmed ? `Для завершения регистрации вам неободимо подтвердить Email.
                                 На вашу почту было отправлено письмо,
                                 перейдите по ссылке в письме для подтверждения почты` :
                                `К данной почте уже привязан аккаунт`;
    });

  }

}


