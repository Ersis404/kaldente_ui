import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorizationComponent } from './authorization/authorization.component';
import { ConfirmEmailCodeComponent } from './authorization/confirm-email-code/confirm-email-code.component';


const routes: Routes = [
  { path: '', component: AuthorizationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class LogInRoutingModule {}
