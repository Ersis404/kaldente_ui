import { NgModule } from '@angular/core';
import { AuthorizationComponent } from './authorization/authorization.component';
import { LogInRoutingModule } from './logIn-routing.module';
import { CommonLibrariesModule } from '../components/common-librari/common-librari.component';
import { ConfirmEmailComponent } from './authorization/confirm-email/confirm-email.component';
import { LogoutComponent } from './logout/logout.component';
import { ConfirmEmailCodeComponent } from './authorization/confirm-email-code/confirm-email-code.component';

@NgModule({
  declarations: [
    AuthorizationComponent,
    ConfirmEmailComponent,
    LogoutComponent,
    ConfirmEmailCodeComponent,
  ],
  imports: [
    LogInRoutingModule,
    CommonLibrariesModule,
  ]
})
export class LogInModule { }
