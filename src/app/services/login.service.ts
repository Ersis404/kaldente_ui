import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, Subject } from 'rxjs';

import { ApiRequestService } from './api-request.service';
import { map } from 'rxjs/operators';
import { UserInfoService, LoginInfoInStorage } from './user-info.service';
import { EventEmitter } from 'protractor';
import { User } from '../models/user/User';

export interface LoginRequestParam {
  username: string;
  password: string;
}

@Injectable()
export class LoginService {
  private isLoginEvent;
  public isLoginEvent$;
  // public landingPage = '/';
  public landingPage = '/CompComponent';


  constructor(
    private router: Router,
    private userInfoService: UserInfoService,
    private apiRequest: ApiRequestService
  ) {
    this.isLoginEvent = new Subject<boolean>();
    this.isLoginEvent$ = this.isLoginEvent.asObservable();
  }

  checkLogin(username: string): Observable<any> {
    return this.apiRequest.get(`api/user/checkLogin/${username}`);
  }

  getToken(user: User): Observable<any> {
    const me = this;

    const login: LoginRequestParam = { username: user.username, password: user.password };

    let loginInfoReturn: LoginInfoInStorage;

    return this.apiRequest.post('login', login, false)
    .pipe(map(jsonResp => {
      if (jsonResp !== undefined && jsonResp !== null) {
        loginInfoReturn = {
          success: true,
          message: jsonResp.operationMessage,
          landingPage: this.landingPage,
          user: {
          userId: jsonResp.item.userId,
          email: jsonResp.item.emailAddress,
          displayName: jsonResp.item.firstName + ' ' + jsonResp.item.lastName,
          token: jsonResp.item.token,
          }
        };

        this.userInfoService.storeUserInfo(JSON.stringify(loginInfoReturn.user));
        this.isLoginEvent.next(true);
      } else {
        loginInfoReturn = {
          success: false,
          message: jsonResp.msgDesc,
          landingPage: '/login'
        };
      }
      return loginInfoReturn;
    }));
  }

  logout(navigatetoLogout = true): void {

    this.userInfoService.removeUserInfo();
    if (navigatetoLogout) {
      this.router.navigate(['/']);
      this.isLoginEvent.next(true);
    }
  }

  register(user: User): Observable<any> {
    return this.apiRequest.post('api/user/registration', user);
  }

  activateEmail(code: string): Observable<any> {
    return this.apiRequest.get(`api/user/activate/${code}`);
  }

}
