import { FormsModule } from '@angular/forms';
import { LoginService } from './services/login.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LogInModule } from './logIn/logIn.module';
import { CommonLibrariesModule } from './components/common-librari/common-librari.component';
import { OperationStatusComponent } from './components/utils/operation-status/operation-status.component';
import { AppConfig } from './app-config';
import { OperationStatusService } from './services/operation-status.service';
import { ApiRequestService } from './services/api-request.service';
import { UserInfoService } from './services/user-info.service';
import { CompComponent } from './comp/comp.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    OperationStatusComponent,
    CompComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LogInModule,
    BrowserAnimationsModule,
    CommonLibrariesModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    AppConfig,
    OperationStatusService,
    ApiRequestService,
    UserInfoService,
    LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
