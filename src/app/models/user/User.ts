export class User {
  userId: string;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  phone: string;
  email: string;
  gender: string;
  role: string;
  secretWord: string;

  constructor(userId?: string,
              username?: string,
              password?: string,
              firstName?: string,
              lastName?: string,
              phone?: string,
              email?: string,
              gender?: string,
              role?: string,
              secretWord?: string) {
                this.userId = userId;
                this.username = username;
                this.password = password;
                this.firstName = firstName;
                this.lastName = lastName;
                this.phone = phone;
                this.email = email;
                this.gender = gender;
                this.role = role;
                this.secretWord = secretWord;
              }
}
